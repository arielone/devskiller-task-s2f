import React from 'react';
import { object,func } from 'prop-types';

export class ContactForm extends React.Component{

    static defaultProps = {
        data:{
            name:'',            
            email:'',
            option:'',
            select: '',
            message:'',
            terms:false
        }
    }

    static propTypes = {
        onChange: func.isRequired,
        onSubmit: func.isRequired,
        data: object.isRequired
    }

    constructor(props){
        super(props)
        this.state={...props.data};
    }

    /**
     * When form is submitted forward contact data to parent
     * @param {event} DOMEvent 
     */
    handleSubmit(event){
        event.preventDefault();

        this.props.onSubmit(this.props.data)
    }
    
    fieldChange(event){
        let target = event.target;
        let value;
        if(target.tagName=="SELECT") value=target.value;
        else if(target.tagName=="TEXTAREA") value=target.value;
        else if(target.tagName=="INPUT") value = target.type ==='checkbox' ? target.checked : target.value;
        let name=target.name;

        this.setState({
            [name]: value
        });

        let data={...this.state.date};
        data[name]=value;
        this.props.onChange(data);
    }

    isSelected(key, option){
        return this.props.data[key] == option
    }

    options = [
        {id:1, label:'I have question about my membership'},
        {id:2, label:'I have technical question'},
        {id:3, label:'I would like to change membership'},
        {id:4, label:'Other question'},
    ]

    render(){
        let data = this.state;

        return <form>

        <h3>Contact Form</h3>

        <div class="form-group">
            <label className="form-label">Your Name:</label>
            <input name="name" className="form-control" value={data.name}  onChange={this.fieldChange.bind(this)} />
        </div>
            
        <div class="form-group">
            <label className="form-label">Your Best Email:</label>
            <input name="email" className="form-control" value={data.email}  onChange={this.fieldChange.bind(this)} />
        </div>
            
        <label className="form-label">Select your membership option:</label>
        <div class="form-group row">
            <label className="form-label col-xs-4">
            <input type="radio" name="option" value="A" checked={data.option=="A"?true:false} onChange={this.fieldChange.bind(this)} /> Option A</label>
            <label className="form-label col-xs-4"> 
            <input type="radio" name="option" value="B" checked={data.option=="B"?true:false} onChange={this.fieldChange.bind(this)} /> Option B</label>
            <label className="form-label col-xs-4"> 
            <input type="radio" name="option" value="C" checked={data.option=="C"?true:false} onChange={this.fieldChange.bind(this)} /> Option C</label>
        </div>

        <hr/>

        <div class="form-group">
            <label className="form-label">What can we help you with:</label>
            <select  className="form-control" name="select" value={data.select} onChange={this.fieldChange.bind(this)}>
                {this.options.map(item=>(
                    <option key={item.id} value={item.id}>{item.label}</option>
                ))}
                <option value="1">I have question about my membership</option>
            </select>
        </div>

        <div class="form-group">
            <label className="form-label">Message:</label>
            <textarea name="message" rows="10" placeholder="Please type your question here"  className="form-control" value={data.message} onChange={this.fieldChange.bind(this)} />
        </div>

        <div class="form-group">
            <label className="form-label"> <input type="checkbox" name="terms" checked={data.terms} onChange={this.fieldChange.bind(this)} /> I agree to terms and conditions </label>

        </div>

            <input type="submit" value="Send" className="contactform-submit" onClick={this.handleSubmit.bind(this)} />
        </form>
    }
}