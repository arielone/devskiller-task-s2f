import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon'
import { shallow, mount, render } from 'enzyme';
import './setup.js'

import { ContactForm } from '../components/contact-form'


describe('ContactForm Component', ()=>{

    var wrapper, contactData, fields, onChange, onSubmit;
    beforeEach(()=>{
        // Initial Form Data
        contactData = {
            name:'Test',
            email:'test@example.com',
            option:'A',
            select: '3',
            message:'Test message',
            terms:true
        }
        // Form callbacks Spies
        onChange = sinon.spy()
        onSubmit = sinon.spy()
        // Form Wrapper
        wrapper = mount(<ContactForm data={contactData} 
                                     onChange={onChange} 
                                     onSubmit={onSubmit}/>);
        // Form Fields wrappers                                     
        fields = {
            name: wrapper.find('[name="name"]'),
            email: wrapper.find('[name="email"]'),
            options: wrapper.find('[name="option"]'),
            select: wrapper.find('[name="select"]'),
            message: wrapper.find('[name="message"]'),
            terms: wrapper.find('[name="terms"]'),
        }
    })
    
    it('should be stateless component',()=>{
        expect(wrapper.instance().state,'don`t use setState() in ContactForm').not.to.exist
    })

    it('should call onSubmit when send button is clicked',()=>{
        wrapper.find('[type="submit"]').simulate('click')
        expect(onSubmit.called).to.be.true
    })

    it('should update `name` field value', ()=>{
         testField('name',fields.name, 'name', 'changed '+Date.now())
    })

    it('should update `email` field value', ()=>{
         testField('email',fields.email, 'email', 'changed '+Date.now())
    })

    function testField(name, field, key , changedValue){
        // Test initial value
        expect(field,`Field '${name}' initial value is missing! `).to.have.value(contactData[key])
        // Change Value
        wrapper.setProps({
            data:{
                ...contactData,
                [key]: changedValue
            }
        })
        wrapper.update()
        // Test changed Value
        expect(field,`Field '${name}' value is not updated correctly! `).to.have.value(changedValue)
    }
})