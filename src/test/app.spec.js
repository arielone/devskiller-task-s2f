import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon'
import { shallow, mount, render } from 'enzyme';
import './setup.js'

import { App } from '../components/app'
import { ContactForm } from '../components/contact-form'
import { Message } from '../components/message'
import { UserPanel } from '../components/user-panel'

describe('App Component', ()=>{
 
    let wrapper;
    beforeEach(()=>{
        wrapper = shallow(<App/>)
    })

    it('should render ContactForm',()=>{
        expect(wrapper.find(ContactForm).exists()).to.be.true
    })

    it('should hide <ContactForm/> after sendContact() was called',()=>{
        wrapper.instance().sendContact({})
        expect(wrapper.find(ContactForm).exists()).not.to.be.true
    })

});